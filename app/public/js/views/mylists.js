$('#Title').autocomplete({
    source: function(req,res) {
        $.ajax({
            url: "https://meantestbed-c9-tvle83.c9.io/autocomplete/" + req.term,
            dataType: "jsonp",
            type: "GET",
            data: {
                term: req.term
            },
            success: function(data) {
                res($.map(data, function(item) {
                    return {
                        label: item.Title,//text comes from a collection of mongo
                        value: item.Title
                    };
                }));
            },
            error: function(xhr) {
                alert(xhr.status + ' : ' + xhr.statusText);
            }
        });
    },
    minLength: 2,
    select: function(event, ui) {
      this.value = ui.item.label;
      $(this).next("input").val(ui.item.value);
      event.preventDefault();
    }
});

$( "#sortable" ).sortable();
$( "#sortable" ).disableSelection();