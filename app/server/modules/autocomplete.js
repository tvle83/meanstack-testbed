
var MongoDB   = require('mongodb').Db;
var Server    = require('mongodb').Server;

var dbPort    = 27017;
var dbHost    = process.env.IP;
var dbName    = 'node-login';
/* establish the database connection */

var db = new MongoDB(dbName, new Server(dbHost, dbPort, {auto_reconnect: true}), {w: -1});
	db.open(function(e, d){
	if (e) {
		console.log(e);
	}	else{
		console.log('connected to database :: ' + dbName);
	}
});

exports.find = function(req, res) {
  var b = req.params.search;
  db.collection('movies', function(err, collection) {
    collection.find({Title:new RegExp(b,'i')}).limit(5).toArray(function(err, items) {
      res.jsonp(items);
    });
  });
};